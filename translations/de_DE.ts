<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>about</name>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>View on GitLab</source>
        <translation>Auf GitLab zeigen</translation>
    </message>
</context>
<context>
    <name>edit_shortcut</name>
    <message>
        <source>Edit shortcut</source>
        <translation>Shortcut bearbeiten</translation>
    </message>
    <message>
        <source>Add shortcut</source>
        <translation>Shortcut hinzufügen</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation>Shortcut</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <source>Click to set shortcut</source>
        <translation>Klicken um Shortcut festzulegen</translation>
    </message>
    <message>
        <source>Select folder to open</source>
        <translation>Wähle den Ordner welcher geöffnet werden soll</translation>
    </message>
    <message>
        <source>No key defined</source>
        <translation>Keine Taste definiert</translation>
    </message>
    <message>
        <source>Please define a key to use for this shortcut!</source>
        <translation>Bitte definiere eine Taste welche für diesen Shortcut verwendet werden soll!</translation>
    </message>
    <message>
        <source>Duplicate shortcut</source>
        <translation>Doppelter Shortcut</translation>
    </message>
    <message>
        <source>Another shortcut for key &apos;{}&apos; already exists!</source>
        <translation>Ein anderer Shortcut verwendet bereits die Taste &apos;{}&apos;!</translation>
    </message>
    <message>
        <source>No action selected</source>
        <translation>Keine Aktion ausgewählt</translation>
    </message>
    <message>
        <source>Please select an action to do!</source>
        <translation>Bitte wähle eine Aktion welche ausgeführt werden soll!</translation>
    </message>
    <message>
        <source>Missing application</source>
        <translation>Fehlende Anwendung</translation>
    </message>
    <message>
        <source>Please select the application to launch!</source>
        <translation>Bitte wähle die Anwendung welche gestartet werden soll!</translation>
    </message>
    <message>
        <source>Missing command</source>
        <translation>Fehlender Befehl</translation>
    </message>
    <message>
        <source>Please specify the command to execute!</source>
        <translation>Bitte gebe den Befehl an welcher ausgeführt werden soll!</translation>
    </message>
    <message>
        <source>Missing folder path</source>
        <translation>Fehlender Ordnerpfad</translation>
    </message>
    <message>
        <source>Please select the path to the folder to open!</source>
        <translation>Bitte wähle den Pfad zu dem Ordner aus welcher geöffnet werden soll!</translation>
    </message>
    <message>
        <source>Missing text</source>
        <translation>Fehlender Text</translation>
    </message>
    <message>
        <source>Please specify the text to input!</source>
        <translation>Bitte gebe den Text an welcher eingegeben werden soll!</translation>
    </message>
    <message>
        <source>Please specify the key sequence to input!</source>
        <translation>Bitte gebe die Tastenfolge an welche eingegeben werden soll!</translation>
    </message>
    <message>
        <source>Edit...</source>
        <translation>Bearbeiten...</translation>
    </message>
    <message>
        <source>An error occurred while parsing the desktop files. Those files will be skipped.</source>
        <translation type="obsolete">Beim Lesen der Desktop Dateien ist ein Fehler aufgetreten. Diese Dateien werden übersprungen.</translation>
    </message>
    <message>
        <source>Errors</source>
        <translation type="vanished">Fehler</translation>
    </message>
    <message>
        <source>Loading desktop files failed</source>
        <translation>Laden der Desktop Dateien fehlgeschlagen</translation>
    </message>
    <message>
        <source>An error occurred while parsing the desktop files.</source>
        <translation type="vanished">Beim Lesen der Desktop Dateien ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <source>Those files will be skipped.</source>
        <translation>Diese Dateien werden übersprungen.</translation>
    </message>
    <message>
        <source>Label options</source>
        <translation>Tastenbeschriftungsoptionen</translation>
    </message>
    <message>
        <source>Icon</source>
        <translation>Symbol</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <source>Click to change color</source>
        <translation>Klicken um die Farbe zu ändern</translation>
    </message>
    <message>
        <source>Open label icon</source>
        <translation>Tastenbeschriftungssymbol auswählen</translation>
    </message>
</context>
<context>
    <name>key_sequence_builder</name>
    <message>
        <source>Build key sequence</source>
        <translation>Tastenfolge erstellen</translation>
    </message>
    <message>
        <source>Add key combination to sequence</source>
        <translation>Tastenkombination zu Tastenfolge hinzufügen</translation>
    </message>
    <message>
        <source>Key combination</source>
        <translation>Tastenkombination</translation>
    </message>
    <message>
        <source>Add key to combination</source>
        <translation>Taste zu Kombination hinzufügen</translation>
    </message>
    <message>
        <source>Remove this key combination</source>
        <translation>Diese Tastenkombination entfernen</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Unable to load shortcuts from config file!</source>
        <translation>Die Shortcuts konnten von der Konfigurationsdatei nicht geladen werden!</translation>
    </message>
    <message>
        <source>Keyboard Mapper is already running!</source>
        <translation>Keyboard Mapper läuft bereits!</translation>
    </message>
</context>
<context>
    <name>main_window</name>
    <message>
        <source>Shortcuts locked, click to unlock</source>
        <translation>Shortcuts gesperrt, klicken zum Entsperren</translation>
    </message>
    <message>
        <source>Remove shortcut</source>
        <translation>Shortcut entfernen</translation>
    </message>
    <message>
        <source>Are you sure to remove the shortcut &apos;{}&apos;?</source>
        <translation type="obsolete">Bist du dir sicher, dass du den Shortcut &apos;{}&apos; entfernen möchtest?</translation>
    </message>
    <message>
        <source>And {} more.</source>
        <translation>Und {} weitere.</translation>
    </message>
    <message>
        <source>Are you sure to remove the selected shortcuts?

{}</source>
        <translation>Bist du dir dicher, dass du die ausgewählten Shortcuts entfernen möchtest?

{}</translation>
    </message>
</context>
<context>
    <name>main_window_menu</name>
    <message>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation>Einstellungen...</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <source>Add shortcut...</source>
        <translation>Shortcut hinzufügen...</translation>
    </message>
    <message>
        <source>Edit shortcut...</source>
        <translation>Shortcut bearbeiten...</translation>
    </message>
    <message>
        <source>Remove shortcut...</source>
        <translation>Shortcut entfernen...</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation>Ausführen</translation>
    </message>
    <message>
        <source>Print labels...</source>
        <translation type="obsolete">Tastenbeschriftungen ausdrucken...</translation>
    </message>
    <message>
        <source>Duplicate shortcut...</source>
        <translation>Shortcut duplizieren...</translation>
    </message>
    <message>
        <source>Print labels for selected shortcuts...</source>
        <translation type="obsolete">Tastenbeschriftungen für ausgewählte Shortcuts ausdrucken...</translation>
    </message>
    <message>
        <source>Print labels</source>
        <translation>Tastenbeschriftungen ausdrucken</translation>
    </message>
    <message>
        <source>All shortcuts...</source>
        <translation>Alle Shortcuts...</translation>
    </message>
    <message>
        <source>Selected shortcuts...</source>
        <translation>Ausgewählte Shortcuts...</translation>
    </message>
    <message>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>main_window_statusbar</name>
    <message>
        <source>1 Shortcut</source>
        <translation>1 Shortcut</translation>
    </message>
    <message>
        <source>{} shortcuts</source>
        <translation>{} Shortcuts</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Enable tray icon</source>
        <translation>Tray Icon aktivieren</translation>
    </message>
    <message>
        <source>Allow only one instance</source>
        <translation>Nur eine Instanz erlauben</translation>
    </message>
    <message>
        <source>Start on login</source>
        <translation>Beim Anmelden starten</translation>
    </message>
    <message>
        <source>Create desktop file</source>
        <translation>Desktop Datei erzeugen</translation>
    </message>
    <message>
        <source>Keyboard input device</source>
        <translation>Tastatureingabegerät</translation>
    </message>
    <message>
        <source>Icon theme</source>
        <translation>Symbol Thema</translation>
    </message>
    <message>
        <source>No keyboard input device selected</source>
        <translation type="vanished">Kein Tastatureingabegerät ausgewählt</translation>
    </message>
    <message>
        <source>Please select at least one input device to use!</source>
        <translation type="vanished">Bitte wähle mindestens ein Eingabegerät aus welches verwendet werden soll!</translation>
    </message>
    <message>
        <source>Labels</source>
        <translation>Tastenbeschriftungen</translation>
    </message>
    <message>
        <source>Default size</source>
        <translation>Standardgröße</translation>
    </message>
    <message>
        <source>Icon margin</source>
        <translation>Symbolabstand</translation>
    </message>
    <message>
        <source>Millimeter</source>
        <translation>Millimeter</translation>
    </message>
    <message>
        <source>Inch</source>
        <translation>Zoll</translation>
    </message>
    <message>
        <source>Pixel</source>
        <translation>Pixel</translation>
    </message>
    <message>
        <source>Measure unit</source>
        <translation>Maßeinheit</translation>
    </message>
    <message>
        <source>Select devices</source>
        <translation>Geräte auswählen</translation>
    </message>
    <message>
        <source>Add device</source>
        <translation>Gerät hinzufügen</translation>
    </message>
    <message>
        <source>Remove device</source>
        <translation>Gerät entfernen</translation>
    </message>
    <message>
        <source>No keyboard input device added</source>
        <translation>Keine Tastatureingabegeräte hinzugefügt</translation>
    </message>
    <message>
        <source>Please add at least one input device to use!</source>
        <translation>Bitte füge mindestens ein Eingabegerät hinzu welches verwendet werden soll!</translation>
    </message>
</context>
<context>
    <name>shortcut_action</name>
    <message>
        <source>Launch application</source>
        <translation type="vanished">Anwendungen starten</translation>
    </message>
    <message>
        <source>Execute command</source>
        <translation type="vanished">Befehl ausführen</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="vanished">Ordner öffnen</translation>
    </message>
    <message>
        <source>Input text</source>
        <translation type="vanished">Text eingeben</translation>
    </message>
    <message>
        <source>Input key sequence</source>
        <translation type="vanished">Tastenfolge eingeben</translation>
    </message>
    <message>
        <source>Lock keys</source>
        <translation type="vanished">Tasten sperren</translation>
    </message>
</context>
<context>
    <name>shortcut_error</name>
    <message>
        <source>Action: {}</source>
        <translation type="vanished">Aktion: {}</translation>
    </message>
    <message>
        <source>An error occurred while executing the action for key {} on device {}!</source>
        <translation type="vanished">Beim Ausführen der Aktion für die Taste {} auf dem Gerät {} ist ein Fehler aufgetreten!</translation>
    </message>
</context>
<context>
    <name>shortcut_list_column</name>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <source>Key</source>
        <translation>Taste</translation>
    </message>
    <message>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <source>Executions</source>
        <translation>Ausführungen</translation>
    </message>
    <message>
        <source>Last execution</source>
        <translation>Letzte Ausführung</translation>
    </message>
</context>
<context>
    <name>shortcut_requester</name>
    <message>
        <source>Configure key</source>
        <translation>Taste konfigurieren</translation>
    </message>
    <message>
        <source>Press the key to use.</source>
        <translation>Drücke die Taste welche verwendet werden soll.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>tray_menu</name>
    <message>
        <source>Show window</source>
        <translation>Fenster zeigen</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
</context>
</TS>
